"""scrap_realty.py <command> [command-opts]

A simple scraper script for www.realtyaustin.com.

Commands
--------

pull_search [search_url]
  Spiders given search page (or default if none provided) pulling all properties
  into local property database.

pull_detail detail_url
  Pulls detail page information into local property database.

print_table
  Prints properties in simple table.

repull_current
  Pull updated data about current candidate properties.

delete_prop [mls number]
  Delete property from data base
"""

import os
import re
import shelve
import sys
import urllib2

from BeautifulSoup import BeautifulSoup
from googlemaps import GoogleMaps

DEFAULT_START = "http://www.realtyaustin.com/idx/homes/46967/search.html"
SHELVE_FILE = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'prop')
WORK_ADDR = '10100 Burnett Rd, Austin, TX'

def pretty_long_table(props):
    print " | ".join(["Days", "Distance", "Year Built", "Price", "Square Ft", "B/B/G", "Address", "MLS", "URL"])
    print 120*'*'
    for prop in props:
        print get_days(prop['status']), int(prop['distance']), \
            prop['Year Built'], prop['price'], prop['Square Ft'],\
            "%s/%s/%s" % (prop['Bedrooms'],prop['Bathrooms'], prop.get('Garage Spaces',0)),\
            prop['address'], prop['mls'], prop['url']

def csv_table(props):
    print ", ".join([ "MLS", "Address", "Square Ft", "Bed","Bath", "Price", "Distance to PRC", "URL"])
    for prop in props:
        print '%(mls)s, "%(address)s", %(Square Ft)s, %(Bedrooms)s, %(Bathrooms)s, %(price)s, %(distance)d, %(url)s' % prop

def pretty_short_table(props):
    print " | ".join(["Address", "MLS"])
    print 120*'*'
    for prop in props:
        print prop['address'], prop['mls']

def get_abs_link(src, ref):
    q_idx = src.find('?')
    if q_idx > -1:
        src = src[:q_idx]
    base_idx = len(src)
    if ref[0] == '/':
        if src[:5] == 'http:':
            idx = src[7:].find('/')
            if idx > -1:
                base_idx = idx + 7
        else:
            raise ValueError("Unable to handle src: %s" % src)
    return src[:base_idx] + ref

def filter_props(prop_dicts, pending=False):
    ret = []
    for prop in prop_dicts:
        if prop.get('address','').find("usswood") != -1:
            ret.append(prop)
            continue
        if int(prop.get('Bedrooms on Main Level', 0)) == 0:
            continue
        if int(prop.get('Bedrooms on Main Level', 0)) < 3 and \
            str(prop.get('Master Main', 'Yes')) == 'Yes':
            continue
        if prop.get('Garages') == "N/A":
            continue
        if not pending and prop.get('status', '').find('Active') == -1:
            continue
        if prop.get('Utilities', '').find('Natural Gas') == -1:
            continue
        if int(prop.get('Square Ft', 0)) < 2000:
            continue
        ret.append(prop)
    return sorted(ret, key=lambda p: p['distance'])

def filter_for_bottom_bedroom(props):
    return filter(lambda x: int(x.get('Bedrooms on Main Level', 0)) >= 1, props)

def prop_from_address(props, addr):
    gmaps = GoogleMaps()
    ret_dict = []
    for prop in props:
        try:
            print "Getting directions to %s" % prop['address']
            directions = gmaps.directions(prop['address'], addr)
            distance = directions["Directions"]["Distance"]["meters"] / 1600.
            ret_dict.append((distance, prop))
        except:
            print "Could not get prop %s directions" % prop.get('mls')
            import traceback; traceback.print_exc()
    ret_dict.sort()
    return ret_dict

def parse_search_page(page):
    print "Visiting search page", page
    start = urllib2.urlopen(page)
    soup = BeautifulSoup(start.read())
    prop_div = soup.find('div', {'id':'idx-results'})
    cd = [d for d in soup.findAll(text=True) if  d.find('link') != -1][0]
    detail_links = set([line.partition('=')[-1].replace(';','').replace("'",'').strip() for line in cd.split('\n') if line.find('link') != -1])
    #    detail_links = [a.get('href') for a in \
    #                prop_div.findAll('a', {'title':'View Property Details'})]
    rel_to_abs = lambda x: get_abs_link(page, x)
    #detail_links = set(map(rel_to_abs, detail_links))
    print detail_links

    div_paging = soup.find('div', {'class':'paging bottom'})
    search_links = [a.get('href') for a in \
                    div_paging.findAll('a')]
    search_links = set(map(rel_to_abs, search_links))
    return detail_links, search_links

def datagroup_to_dict(soup):
    keys = [k.getText().replace(':', '') for k in soup.findAll('span',{'class':'key'})]
    vals = [v.getText() for v in soup.findAll('span',{'class':'val'})]
    return dict(zip(keys, vals))

def parse_detail_page(page):
    print "Visiting detail page", page
    ret_dict = {}
    try:
        soup = BeautifulSoup(urllib2.urlopen(page).read())
    except urllib2.HTTPError:
        return {}
    summary = soup.find('div', {'id':'idx-detail-primary'})
    ret_dict['url'] = page
    ret_dict['address'] = summary.find('h1').getText().replace('\t','').replace('\r','').replace('\n', ' ')
    h2_spans = summary.find('h2').findAll('span')
    ret_dict['price'] = int(h2_spans[0].getText().replace('$','').replace(',',''))
    ret_dict['mls'] = h2_spans[1].getText().replace('MLS #: ','')
    ret_dict['status'] = h2_spans[2].getText().replace('&nbsp;', '').replace('&ndash;','').strip()
    ret_dict.update(datagroup_to_dict(summary.find('div',{'class':'idx-datagrp'})))
    secondary = soup.find('div',{'id':'idx-detail-secondary'})
    map(lambda d: ret_dict.update(d), map(datagroup_to_dict, secondary.findAll('div',{'class':'idx-dataset'})))
    return ret_dict

def update_distances(prop_dicts):
    gmaps = GoogleMaps()
    for prop in prop_dicts:
        if not prop:
            continue
        if prop.get('distance') is None:
            try:
                print "Getting directions to %s" % prop['address']
                directions = gmaps.directions(prop['address'], WORK_ADDR)
                prop['distance'] = directions["Directions"]["Distance"]["meters"] / 1600.
            except:
                print "Could not get prop %s directions" % prop.get('mls')
                import traceback; traceback.print_exc()

def shelve_results(prop_dicts):
    print "Saving results"
    shelf = shelve.open(SHELVE_FILE)
    for prop in prop_dicts:
        if not prop:
            continue
        shelf[str(prop['mls'])] = prop
    shelf.close()

def get_shelved_props():
    shelf = shelve.open(SHELVE_FILE)
    ret = shelf.values()
    shelf.close()
    return ret

def get_days(s):
    return 0
    m = re.match(".* (?P<days>\d+) Days on Website", s)
    g = m.groups('days')
    return int(g[0]) if len(g) > 0 else -1

def pull_search(search_url):
    search_links = set()
    search_links.add(search_url)
    visited_search = set()
    detail_links = set()
    detail_dicts = []
    while search_links:
        search = search_links.pop()
        new_details, new_search = parse_search_page(search)
        visited_search.add(search)
        detail_links.update(new_details)
        search_links.update(new_search - visited_search)
    for detail_link in detail_links:
        detail_dicts.append(parse_detail_page(detail_link))
    update_distances(detail_dicts)
    shelve_results(detail_dicts)

def pull_detail(*detail_urls):
    detail_dicts = [parse_detail_page(detail_url) for detail_url in detail_urls]
    update_distances(detail_dicts)
    shelve_results(detail_dicts)

def repull_current():
    """Gets data for items that we are currently considering"""
    props = get_shelved_props()
    props = filter_props(props, pending=True)
    map(pull_detail, [prop['url'] for prop in props])

def delete_prop(mls):
    print("Deleting prop", mls)
    shelf = shelve.open(SHELVE_FILE)
    del shelf[mls]
    shelf.close()

def print_usage_and_exit():
    print __doc__
    sys.exit(1)

def print_current():
    props = get_shelved_props()
    props = filter_props(props)
    #props = filter_for_bottom_bedroom(props)
    #pretty_long_table(props)
    csv_table(props)

if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print_usage_and_exit()
    elif sys.argv[1] == "pull_search":
        try:
            pull_search(sys.argv[2] if len(sys.argv) == 3 else DEFAULT_START)
        except:
            import pdb; pdb.set_trace()
    elif sys.argv[1] == "pull_detail":
        #import pdb; pdb.set_trace()
        pull_detail(*sys.argv[2:])
    elif sys.argv[1] == "print_table":
        print_current()
    elif sys.argv[1] == "repull_current":
        repull_current()
    elif sys.argv[1] == "delete_prop":
        delete_prop(sys.argv[2])
    else:
        print_usage_and_exit()
